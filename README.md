# Sample Express App


# Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- `npm start` to start the local server
- `npm run dev` to start the local server in dev mode

# Unit Tests

The unit tests are all kept in the `test` folder.

To manually run the in-proc unit tests, run:

```bash
npm test
```

This will run `mocha`, executing tests in the `test/` folder.

# Code Coverage

Code coverage reports can be generated with:

```bash
npm run test:coverage
```

Reports are written to the screen and to the `coverage` directory.
