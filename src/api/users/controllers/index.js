const request = require('request-promise-native');

/**
 * Get list users handler
 *
 * @param {object} req - express request object
 * @param {object} res - express response object
 * @returns {Array<object>} - contains list of found users
 */
const listUsers = async (req, res) => {
  // get users from sample public API
  const options = {
    url: 'https://jsonplaceholder.typicode.com/users',
    resolveWithFullResponse: false,
    json: true
  };
  const users = await request.get(options);
  // return list of found users
  res.send(users);
};

module.exports = {
  listUsers
};
