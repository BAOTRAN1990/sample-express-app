const express = require('express');

const { listUsers } = require('./controllers');
const { tryAsync } = require('../../common/utils');

const router = express.Router();

router.get('/users', tryAsync(listUsers));

module.exports = router;
