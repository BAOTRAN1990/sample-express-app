const express = require('express');

const users = require('./api/users');
const globalErrorHandler = require('./common/errors/globalErrorHandler');

const app = express();

app.use(users);
// config global error handler
app.use(globalErrorHandler);

module.exports = app;
