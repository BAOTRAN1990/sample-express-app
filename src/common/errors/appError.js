const Boom = require('boom');
const ExtendableError = require('es6-error');

/**
 * Base error class for application
 */
class AppError extends ExtendableError {
  constructor(statusCode = 500, message) {
    super(message);
    this.statusCode = statusCode;
  }

  /**
   * Convert App Error to JSON
   * It would result by getting output payload from Boom object
   *
   * @param
   * @returns {object} - error json object with boom format
   */
  toJSON() {
    const { output: { payload } } = new Boom(this.message, { statusCode: this.statusCode });
    return payload;
  }
}

module.exports = AppError;
