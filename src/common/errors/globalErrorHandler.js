const AppError = require('./appError');

/**
 * Global error handler function which return error response to client
 * Here is error response structure (using Boom format):
 *  {
 *    statusCode
 *    error
 *    message
 *  }
 */
const globalErrorHandler = (err, req, res, next) => {
  let error;

  // handle AppError response which is thrown from handler
  if (err instanceof AppError) {
    error = err;
  } else {
    // start handling error response from request promise native library (when invoking http call)
    // get status code and message from err object returned by request promise native library
    const { statusCode, message } = err;
    // instantiate an AppError with status code and message
    error = new AppError(statusCode, message);
  }

  res.status(error.statusCode).json(error.toJSON());

  next();
};

module.exports = globalErrorHandler;
