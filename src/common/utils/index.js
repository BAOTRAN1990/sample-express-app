/**
 * This method is used to catched error from async handler process and go to global error handler
 */
const tryAsync = fn => async (req, res, next) => {
  try {
    await fn(req, res);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  tryAsync
};
