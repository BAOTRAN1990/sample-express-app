const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const request = require('request-promise-native');

chai.use(sinonChai);
const { expect } = chai;

const { listUsers } = require('../../../../src/api/users/controllers');

const sampleListUsers = [{
  id: 1,
  name: 'Leanne Graham',
  username: 'Bret',
  email: 'Sincere@april.biz',
  address: {
    street: 'Kulas Light',
    suite: 'Apt. 556',
    city: 'Gwenborough',
    zipcode: '92998-3874',
    geo: {
      lat: '-37.3159',
      lng: '81.1496'
    }
  },
  phone: '1-770-736-8031 x56442',
  website: 'hildegard.org',
  company: {
    name: 'Romaguera-Crona',
    catchPhrase: 'Multi-layered client-server neural-net',
    bs: 'harness real-time e-markets'
  }
}];

describe('Users controllers', () => {

  describe('listUsers', () => {

    let mockRes;

    beforeEach(() => {
      request.get = sinon.stub().resolves(sampleListUsers);
      mockRes = {
        send: sinon.stub()
      };
    });

    it('should call get method of request promise native to get users from public api', async () => {
      const expectedGetOptions = {
        url: 'https://jsonplaceholder.typicode.com/users',
        resolveWithFullResponse: false,
        json: true
      };
      await listUsers(null, mockRes);

      expect(request.get).to.be.calledOnceWith(expectedGetOptions);
    });

    it('should send list of found users to response', async () => {
      await listUsers(null, mockRes);

      expect(mockRes.send).to.be.calledOnceWith(sampleListUsers);
    });

    it('should not call send method of res if failed to get users from public api', async () => {
      request.get = sinon.stub().rejects(new Error());

      return listUsers(null, mockRes)
        .catch(() => expect(mockRes.send.callCount).to.equal(0));
    });

  });

});
