const chai = require('chai');
const Boom = require('boom');

const { expect } = chai;

const AppError = require('../../../src/common/errors/appError');

describe('AppError', () => {

  describe('constructor', () => {

    it('should set default status code if it does not exist', () => {
      const error = new AppError();

      expect(error.statusCode).to.equal(500);
    });

    it('should set status code and message properly when instantiate an AppError', () => {
      const error = new AppError(400, 'Bad Request');

      expect(error.statusCode).to.equal(400);
      expect(error.message).to.equal('Bad Request');
    });

  });

  describe('toJSON', () => {

    it('should return payload of Boom object', () => {
      const error = new AppError(400, 'Bad Request');
      const { output: { payload: expectedResult } } = new Boom('Bad Request', { statusCode: 400 });

      expect(error.toJSON()).to.deep.equals(expectedResult);
    });

  });

});
