const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const Boom = require('boom');

chai.use(sinonChai);
const { expect } = chai;

const globalErrorHandler = require('../../../src/common/errors/globalErrorHandler');
const AppError = require('../../../src/common/errors//appError');

describe('Global Error Handler', () => {

  let sendJSONResultStub;
  let setStatusCodeStub;
  let next = sinon.stub();
  let mockRes;

  beforeEach(() => {
    sendJSONResultStub = sinon.stub();
    setStatusCodeStub = sinon.stub();
    next = sinon.stub();
    mockRes = {
      status: setStatusCodeStub.returns({
        json: sendJSONResultStub
      })
    };
  });

  it('should return error response correctly when error is an AppError thrown in handler', () => {
    const error = new AppError(500, 'Internal Server Error');
    globalErrorHandler(error, null, mockRes, next);

    expect(setStatusCodeStub).to.be.calledOnceWith(500);
    expect(sendJSONResultStub).to.be.calledOnceWith(error.toJSON());
  });

  it('should return error response correctly when error is an error thrown from request promise native library', () => {
    const error = {
      statusCode: 404,
      message: 'Not found'
    };
    const {
      output: {
        payload: expectedErrorResponse
      }
    } = new Boom(error.message, { statusCode: error.statusCode });
    globalErrorHandler(error, null, mockRes, next);

    expect(setStatusCodeStub).to.be.calledOnceWith(error.statusCode);
    expect(sendJSONResultStub).to.be.calledOnceWith(expectedErrorResponse);
  });

});
