const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
const { expect } = chai;

const { tryAsync } = require('../../../src/common/utils');
const AppError = require('../../../src/common/errors/appError');

describe('Utils', () => {

  describe('tryAsync', () => {
    let next;

    beforeEach(() => {
      next = sinon.stub();
    });

    it('should call next with error if there is rejected promise while executing fn', async () => {
      const error = new AppError(400, 'Bad Request');
      const fn = sinon.stub().rejects(error);

      await tryAsync(fn)(null, null, next);

      expect(next).to.be.calledOnceWith(error);
    });

    it('should not call next with error if there is no rejected promise while executing fn', async () => {
      const fn = sinon.stub().resolves();
      await tryAsync(fn)(null, null, next);

      expect(next).to.have.callCount(0);
    });
  });

});
